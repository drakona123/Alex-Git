package lecture03.task00;

import java.awt.*;
import java.util.Scanner;

//
public class PersonCharacteristics {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter number of people:");
        int input = scanner.nextInt();
        String FirstName="";
        String LastName= "";
        int YearOfBirth = 0;
        int Weight = 0;
        int Height = 0;
        String Job = "";
        String printall="";
        for (int i = 0; i < input; i++) {


            do {
                FirstName = scanner.next();
            } while(FirstName.length() == 0);
            do {
                LastName = scanner.next();
            } while (LastName.length() == 0);
            do {
                YearOfBirth = scanner.nextInt();
                if (YearOfBirth <= 1900 || YearOfBirth >= 2018){
                    System.out.println("Enter a year bigger than 1900 and smaller than 2018.");
                }
            } while(YearOfBirth <= 1900 || YearOfBirth >= 2018);
            do {
                Weight = scanner.nextInt();
            } while (Weight <= 10 || Weight > 280);
            do {
                Height = scanner.nextInt();
            } while (Height <= 25 || Height > 250);
            do {
                Job = scanner.next();
            } while (Job.length() == 0);
            printall += FirstName + " " + LastName + " " + "is" + " " + " " + (2018 - YearOfBirth) + " " + "years old.His weight is" + " " + Weight + " " + "kg and he is" + " " + Height + "cm tall.He is a" + " " + Job + ".";
            if (YearOfBirth <= 18) {
                printall += FirstName +" " + LastName + " is under-aged.";
            }
        }
        System.out.println(printall);
    }
}
