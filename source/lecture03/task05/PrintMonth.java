package lecture03.task05;

import java.util.Scanner;

public class PrintMonth {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Input a month");
        int Month = scanner.nextInt();
        if(Month <= 0 || Month>=13){
            System.out.println("Error:Your number of month needs to be between 1-12");
            } else {
            if(Month == 1) {
                System.out.println("January");
            } else if(Month == 2) {
                System.out.println("February");
            } else if(Month == 3) {
                System.out.println("March");
            } else if(Month == 4) {
                System.out.println("April");
            } else if(Month == 5) {
                System.out.println("May");
            } else if(Month == 6) {
                System.out.println("June");
            } else if (Month == 7) {
                System.out.println("July");
            } else if (Month == 8) {
                System.out.println("August");
            } else if (Month == 9) {
                System.out.println("September");
            } else if (Month == 10) {
                System.out.println("Octomber");
            } else if (Month == 11) {
                System.out.println("November");
            } else if (Month == 12) {
                System.out.println("December");

            }
        }
    }
}
