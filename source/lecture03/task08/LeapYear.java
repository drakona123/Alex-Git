package lecture03.task08;

import java.util.Scanner;

public class LeapYear {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter a year");
        int Year = scanner.nextInt();
        if((Year % 100 == 0 && Year % 400 == 0) ^ Year % 4 == 0) {
            System.out.println("true");
        } else {
            System.out.println("false");
        }
    }
}
