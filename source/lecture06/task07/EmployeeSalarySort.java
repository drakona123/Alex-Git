package lecture06.task07;

import java.util.Scanner;

public class EmployeeSalarySort {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int numberOfPeople = sc.nextInt();
        sc.nextLine();
        Employee[] employees = new Employee[numberOfPeople];


        for (int i =0;i < numberOfPeople;i++){
            String input = sc.nextLine();
            Employee employee = createEmployeeFromInput(input);
            employees[i] = employee;
        }
        sortEmployeeBySalary(employees);

        //printTopThreeEmployees(employees);
    }

    private static void sortEmployeeBySalary(Employee[] employees) {
        System.out.println(employees[0]);
    }

    private static Employee createEmployeeFromInput(String input) {

        String[] inputFields = input.split(",");
        String name = inputFields[0];
        double salary = Double.valueOf(inputFields[1].trim());
        String position = inputFields[2];
        String department = inputFields[3];
        Employee employee = new Employee(name,salary,position,department);

        if (inputFields.length >= 5){
            employee.age = Integer.valueOf(inputFields[4].trim());
        }
        if(inputFields.length >= 6){
            String email = inputFields[5];
            employee.email = email;
        }
       return employee;
    }
}
