package lecture06.task07;

public class Employee {
    final String name;
    final double salary;
    final String position;
    final String department;
    int age;
    String email;

    Employee (String name,double salary,String position,String department,int age,String email){
        this.name = name;
        this.salary = salary;
        this.position = position;
        this.department = department;
        this.age = age;
        this.email = email;
    }
    Employee (String name,double salary,String position,String department,int age){
        this.name = name;
        this.salary = salary;
        this.position = position;
        this.department = department;
        this.age = age;

    }
    Employee (String name,double salary,String position,String department,String email){
        this.name = name;
        this.salary = salary;
        this.position = position;
        this.department = department;
        this.email = email;
    }
    Employee (String name,double salary,String position,String department){
        this.name = name;
        this.salary = salary;
        this.position = position;
        this.department = department;

    }
}
