package lecture06.task06;

public class SwiftDate {
    int day;
    int month;
    int year;

    SwiftDate(int year,int month,int day){
        this.year = year;
        this.month = month;
        this.day = day;
    }
    boolean isLeapYear(){
        if ((year %4 == 0 && year %100 != 0) || year %400 == 0) {
            return true;

        }else
            return false;
    }
    int getCentury(){
        return this.year / 100 +1;

    }
    String getInfo(){
        String leapYearTemplate ="";

        if (isLeapYear()){
            leapYearTemplate = "{It is LEAP year.}";
        }else
            leapYearTemplate = "{It is not LEAP year.}";
        int currentCentury = getCentury();
        String info =String.format("%d %d %d - %d century.%s",
                year,month,day,currentCentury,leapYearTemplate);
        return info;

    }
}
