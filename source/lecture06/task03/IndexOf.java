package lecture06.task03;

public class IndexOf {
    public static void main(String[] args) {

        int[] arr1 = {5, 6, 7, 9, 8};
        int[] arr2 = {1,1,1};

        int index1 = indexOf(arr1, 9);
        int index2 =indexOf(arr2, 1);
        System.out.println(index1);
        System.out.println(index2);
    }

    private static int indexOf(int[] numbers, int numberToLookFor) {
        for (int i = 0; i < numbers.length; i++) {
            if (numberToLookFor == numbers[i]) {
                return i;

            }

        }
        return -1;
    }

}
