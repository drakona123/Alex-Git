package lecture06.lecture06.task02;

public class AreEqual {
    public static void main(String[] args) {
        boolean result1 = areEqual(2, 7);
        boolean result2 = areEqual(6, 6);

        System.out.println(result1);
        System.out.println(result2);
    }

    private static boolean areEqual(int firstnum, int secondnum) {
        if (firstnum == secondnum) {
            return true;
        }
        return false;
    }
}
