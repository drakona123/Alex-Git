package lecture06.task01;

public class Printing {
    public static void main(String[] args) {
        printDetails("Stoyan", 29);
    }

    private static void printDetails(String name, int years ) {
        System.out.println(name +" is " + years +" old.");
    }
    }


