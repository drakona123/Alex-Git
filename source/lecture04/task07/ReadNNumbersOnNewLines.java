package lecture04.task07;

import java.util.Scanner;

public class ReadNNumbersOnNewLines {
    public static void main(String[] args) {
        System.out.println("Input number:");
        Scanner sc = new Scanner(System.in);
        int input = sc.nextInt();
        String NumOnSingleLine = "";
        for(int num=0;num<input;num++){
            if(num>0){
                sc.nextLine();
            }
            NumOnSingleLine +=sc.nextInt()+" ";
        }
        System.out.println(NumOnSingleLine);

        }

    }



