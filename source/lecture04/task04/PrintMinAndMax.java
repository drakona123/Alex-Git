package lecture04.task04;

import java.util.Scanner;

public class PrintMinAndMax {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int N = scanner.nextInt();
        int min = 0;
        int max = 0;

        for(int i = 0; i < N; i++){
            int tmp = scanner.nextInt();
            if(i == 0) {
                min = tmp;
                max = tmp;
            }
            if(min > tmp){
                min = tmp ;
            }
            if(max < tmp){
                max = tmp;
            }
        }

        System.out.println(max);
        System.out.println(min);
    }
}
