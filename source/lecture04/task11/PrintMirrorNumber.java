package lecture04.task11;

import java.util.Scanner;

public class PrintMirrorNumber {
    public static void main(String[] args) {
        int num = 0;
        int reversnum = 0;
        System.out.println("Enter  number");
        Scanner sc = new Scanner(System.in);
        num = sc.nextInt();
        for( ; num != 0; ){
            reversnum = reversnum * 10;
            reversnum = reversnum + num%10;
            num = num/10;
        }
        System.out.println("Reverse of your number is:" +" " +reversnum);
    }
}