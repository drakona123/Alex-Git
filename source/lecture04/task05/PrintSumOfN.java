package lecture04.task05;

import java.util.Scanner;

public class PrintSumOfN {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Input number");
        int input = scanner.nextInt();
        int sum = 0;
        for(int i = 0; i < input; i++){
         int tmp = scanner.nextInt();
            sum += tmp;
        }
        System.out.println(sum);
    }
}
