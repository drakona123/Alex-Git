package lecture04.task06;

import java.util.Scanner;

public class ReadNNumbersOnASingleLine {
    public static void main(String[] args) {
        System.out.println("Input number:");
        Scanner sc = new Scanner(System.in);
        int input = sc.nextInt();

        for (int num = 0; num < input; num++) {

            System.out.println(sc.nextInt());

        }
    }
}