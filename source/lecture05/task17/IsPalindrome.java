package lecture05.task17;

import java.util.Scanner;

public class IsPalindrome {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String ST = sc.nextLine();
        String Reversed = "";
        boolean isPalindrome;
        for(int i = ST.length()-1;i >= 0 ;i--){
            Reversed += ST.charAt(i);
        }
        isPalindrome = (ST.equals(Reversed));
        System.out.println(isPalindrome);
    }
}
