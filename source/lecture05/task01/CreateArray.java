package lecture05.task01;

//Напишете програма lecture05.task01.CreateArray,
// която да създава масив от тип int и да го инициализира
//        със стойностите 5, 9, 11, 3, 6, 4, 7. Отпечатайте стойностите на екрана, всяка на нов ред.
public class CreateArray {
    public static void main(String[] args) {
        int[] Array = {5, 9, 11, 3, 6, 4, 7};
        for (int i = 0; i < Array.length;i++){
            System.out.println(Array[i]);
        }

    }

}

