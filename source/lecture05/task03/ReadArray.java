package lecture05.task03;

import java.util.Scanner;

//Напишете програма lecture05.task03.ReadArray, която да създава масив от тип int и да го инициализира със стойности,
// въведени от стандартния вход. На първия ред на стандартния вход ще бъде въведено число N,
// което ще указва броя елементи, които ще бъдат въведени след това. На втория ред стандартния вход ще бъдат
// въведени N на брой числа, разделени с интервал. Нека създаденият масив да има големина точно N. Отпечатайте
// въведените числа, разделени със запетая, на стандартния изход.
public class ReadArray {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        int N = sc.nextInt();
        int[] Array = new int[N];
        for (int i = 0; i <= N; i++) {
            Array[i] = sc.nextInt();

            if(i != N-1) {
                System.out.print(Array[i]+",");
            }else {
                System.out.print(Array[i]);
            }

        }


    }
}
