package lecture05.task02;
//Напишете програма lecture05.task02.CreateStringArray,която да създава масив от тип String и да
// го инициализира с 4 низа - имена на хора.
// Програмата да разпечатва на нов ред всеки един от елементите на масива.

public class CreateStringArray {
    public static void main(String[] args) {
        String[] Names ={"George","Peter","Katy","Alice"};
//        String[] Names = new String[4];
//        Names[0] = "George";
//        Names[1] = "Peter";
//        Names[2] = "Katy";
//        Names[3] = "Alice";
        for(int i = 0; i < Names.length;i++){
            System.out.println(Names[i]);
        }

    }
}
