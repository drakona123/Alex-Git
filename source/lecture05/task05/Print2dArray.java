package lecture05.task05;

import java.util.Scanner;

//Напишете програма lecture05.task05.Print2dArray, която да чете от стандартния вход едно число N и да отпечатва
//матрица NxN с числата от 1 до N*N, следвайки формата:
public class Print2dArray {
    public static void main(String[] args) {
        //Reading from the console number n
        Scanner sc = new Scanner(System.in);
        int counter = 1;
        int N = sc.nextInt();
        for (int row = 1; row <= N ; row++) {
           for(int column =1;column <= N;column++){
               System.out.print(counter+" ");

               if(counter %N == 0){
                   System.out.println();
               }counter++;
           }
        }
    }
}