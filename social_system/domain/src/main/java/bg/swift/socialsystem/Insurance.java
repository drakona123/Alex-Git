package bg.swift.socialsystem;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
    public class Insurance {
        @Id
        @GeneratedValue
        private Integer id_ins;
        private Double amount;
        private String month;
        private String year;

    Insurance() {

    }

    Insurance(Double amount, String year, String month) {
        this.amount = amount;
        this.year = year;
        this.month = month;
    }
    @Override
    public String toString() {
        return "id:" + this.id_ins + "amount: "+ this.amount + "year:" + this.year + "month:" + this.month;
    }
}
