package bg.swift.socialsystem;

import org.springframework.stereotype.Component;

@Component
class PersonService {
    PersonsRepository repository;
    PersonService(PersonsRepository rep){
        repository = rep;
    }

   void printAllPeople() {
        Iterable<Person> tasks = repository.findAll();
        for(Person person : tasks){
            System.out.println(person);
        }

    }
    void addPerson(Person person){
        repository.save(person);
    }
}
