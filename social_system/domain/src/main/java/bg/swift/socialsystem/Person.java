package bg.swift.socialsystem;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

@Entity
public class Person {

    @Id
    @GeneratedValue
    private Integer id;
    private String firstName;
    private String lastName;
    private Character gender;
    private Double height;
    private String dateOfBirth;
    private String country;
    private String city;
    private String municipality;
    private Integer postCode;
    private String streetName;
    private Integer streetNumber;
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Education> educations;
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @Fetch(value = FetchMode.SUBSELECT)
    private List<Insurance> insurances;

        Person() {
            //requested by Hibernate

    }
    Person(String firstName, String lastName, Character gender, Double height, String dateOfBirth, String country, String city, String municipality, Integer postCode, String streetName, Integer streetNumber, List<Education> educations, List<Insurance> insurances) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.gender = gender;
        this.height = height;
        this.dateOfBirth = dateOfBirth;
        this.country = country;
        this.municipality = municipality;
        this.postCode = postCode;
        this.streetName = streetName;
        this.streetNumber = streetNumber;
        this.educations = educations;
        this.insurances = insurances;
    }

    @Override
    public String toString() {
        return this.id + " Fn: " + this.firstName + " Ln:" + this.lastName + " " + this.height + " " + this.educations + " " + this.insurances;
    }


}