public class Post {
    String postedBy;
    String postContent;
    String postDate;

    Post(String postedBy, String postContent, String postDate){
        this.postedBy = postedBy;
        this.postContent = postContent;
        this.postDate = postDate;
    }
}

